## Užitečné nástroje

 - IDE
   - [Pycharm](https://www.jetbrains.com/pycharm/download/#section=linux) s [pluginem](https://plugins.jetbrains.com/plugin/7808-hashicorp-terraform--hcl-language-support)
   - [Visual Studio Code](https://code.visualstudio.com/) s [pluginem](https://marketplace.visualstudio.com/items?itemName=mauve.terraform)
 - credencials manager (optional for aws)
   - [Vaulted](https://github.com/miquella/vaulted) - příklad [nastavení](https://wiki.lukapo.cz/tieto:telia-start#aws_via_command_line)

## Workflow

 - Aktualizujeme git repozitář pomocí `git fetch && git pull`
 - Vytvoříme novou větev `git checkout -b [fix|feature|...]/name_of_branch`
 - Provedeme změny v Terraformu
 - Pokud je to nutné, tak se přihlásíme pomocí vaulted do správného aws profilu
 - Provedeme `terraform plan`
 - Pokud jsme se změnami spokojeni a víme co děláme, tak provedeme deploy změn `terraform apply`
 - Pokud vše proběhlo v pořádku a tak jak mělo, tak provedeme push do repozitáře `git add . && git commit -m "message" && git push origin [fix|feature|..../name_of_branch`
 - Následně už jen provedeme merge request a doufáme, že nám bude schválen
 
## Struktura Terraform repozitáře

 - monorepo (primary for lukapo)
   - Každý projekt (zákazník má svoji složku)
   - V této složce jsou podsložky:
     - init - slouží pro inicializaci ukládání `tfstate` do cloudu - dobrá věc při práci v týmu
     - enviroment (develop, staging, testing, production) - záleží jaké prostředí zákazník požaduje
     - scripts (optional) - pokud se používají scripty, které lze aplikovat na všechny prostředí
     - templates (optional) - pokud se používají templaty, které lze aplikovat na všechny prostředí
 - repozitář zákazníka / projektu
     - init - slouží pro inicializaci ukládání `tfstate` do cloudu - dobrá věc při práci v týmu
     - enviroment (develop, staging, testing, production) - záleží jaké prostředí zákazník požaduje
     - scripts (optional) - pokud se používají scripty, které lze aplikovat na všechny prostředí
     - templates (optional) - pokud se používají templaty, které lze aplikovat na všechny prostředí

## Best practices pro psaní v Terraformu
 
 - Vždy je nutné se vyvarovat rozsáhlým souborům, je lepší soubor raději rozdrobit na vícero menších souborů (dle logických bloků)
 - Pokud rozdrobením způsobíme chaos, je lepší použít modul nebo ho vytvořit
   - Než se pustíme do psaní vlastního modulu, tak si prohlídneme [Terraform registry](https://registry.terraform.io/), preferovaný autor je [**antonbabenko**](https://registry.terraform.io/modules/antonbabenko)
   - Pokud najdeme přibližne to, co potřebujeme, tak vytvoříme lukapo modul s tím, že vybraný modul obohatíme o potřebné featury a daný modul uložíme v [místě tomu určeném](https://gitlab.com/lukapo/terraform/)
   - Obecně platí, že se snažíme psát čištý kód s případným komentářem, který je rozdělen do logických bloků
   - Vždy máme k modulu dopsanou dokumentaci !! (nebo si ji necháme vygenerovat)
 - Pokud potřebujeme pracovat s globálními variables, pak použijeme definici local variable v souboru `env-vars.rf`, tímto způsobem se stávají globální
 - Vždy vyčleníme soubor `providers.tf` na deklaraci věcí k providerovi
 - co nejčastěji využíváme HcL místo json syntaxe
 - pokud máme nadefinovat něco pomocí json, tak použijeme template a render místo přímého vložení do terraform kódu


## Užitečné příkazy pro práci s Terraformem

 - `terraform init` - inicializace všech modulů a providerů, které jsou potřeba
 - `terraform plan` - ukáže změny, které se případně provedou oproti aktuálnímu stavu
 - `terraform apply` - provede změny a stav změn zapíše do `tfstate` souboru
 - `terraform validate` - provede kontrolu syntaxe tf kódu
 - `terraform output` - vypíše výstupy proměnných 
 - `terraform destroy` - zruší veškeré změny, které jsou v Terraform kódu uskutečněny
 - ke každému příkazu lze použít `-target=` a tím vymezit rozsah působnosti, nejčastěji se to využívá na moduly `-target=module.name_of_module`
